<link rel="stylesheet" type="text/css" href="{{ URL::asset('packages/studyx/pages/styles.css') }}" />

<div id="pages">

    @foreach ($lines as $line)

    <div class="lines">
        <span class="title">Line #{{ $line['title'] }}</span>

        <div class="info">
            {{ $line['info'] }}
        </div>
    </div>

    @endforeach

    <div class="count">
        <strong>Total :</strong> {{ $count }} pages.
    </div>

</div>