<?php namespace Studyx\Pages;

use Illuminate\Support\ServiceProvider;

class PagesServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('studyx/pages');

        include __DIR__.'/../../routes.php';

        $app = $this->app;

        // Or us Config::get()
        // And include the class using use Config;

        if ($app['config']->get('page::enabled', true)) {
            echo $app['page']->generateReport();
        }
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->app['page'] = $this->app->share(function($app)
        {
            return new Page($app['view'], $app['config']);
        });

    }

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('page');
	}

}
