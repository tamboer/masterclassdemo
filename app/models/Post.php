<?php

class Post extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'title'   => 'required',
    //    'content' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [
        'title',
        'content'
    ];

    public function user() {
        return $this->belongsTo('User');
    }

    public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function getCommentCount()
    {
        return count($this->comments);
    }

//    function __toString()
//    {
//        return $this->title;
//    }
}