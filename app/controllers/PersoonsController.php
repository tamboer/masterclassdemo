<?php

class PersoonsController extends \BaseController {

	/**
	 * Display a listing of persoons
	 *
	 * @return Response
	 */
	public function index()
	{
		$persoons = Persoon::all();

		return View::make('persoons.index', compact('persoons'));
	}

	/**
	 * Show the form for creating a new persoon
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('persoons.create');
	}

	/**
	 * Store a newly created persoon in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Persoon::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Persoon::create($data);

		return Redirect::route('persoons.index');
	}

	/**
	 * Display the specified persoon.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$persoon = Persoon::findOrFail($id);

		return View::make('persoons.show', compact('persoon'));
	}

	/**
	 * Show the form for editing the specified persoon.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$persoon = Persoon::find($id);

		return View::make('persoons.edit', compact('persoon'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$persoon = Persoon::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Persoon::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$persoon->update($data);

		return Redirect::route('persoons.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Persoon::destroy($id);

		return Redirect::route('persoons.index');
	}

}