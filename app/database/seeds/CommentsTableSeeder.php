<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CommentsTableSeeder extends Seeder {

	public function run()
	{
        $posts = Post::all();
        $users = User::all();
		$faker = Faker::create();

        foreach ($posts as $post) {
            foreach ($users as $user) {
                foreach(range(1, 3) as $index)
                {
                    Comment::create([
                            'content' => $faker->paragraph(2),
                            'active' => $faker->boolean(),
                            'post_id' => $post->id,
                            'user_id' => $user->id
                    ]);
                }
            }
        }
	}

}