<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CategoriesTableSeeder extends Seeder {

    const NUMBER_MAX_CATEGORIES = 10;

    public function run()
	{
		$faker = Faker::create();

		foreach(range(1, self::NUMBER_MAX_CATEGORIES) as $index)
		{
			Category::create([
                'name' => $faker->word,
                'active' => $faker->boolean()
			]);
		}
	}

}