@extends('layouts.bootstrap')

@section('content')
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <td>Naam</td>
        <td>Voornaam</td>
    </tr>
    </thead>
    <tbody>
@foreach($persoons as $key => $value)
<tr>
    <td>{{ $value->naam }}</td>
    <td>{{ $value->voornaam }}</td>
    <!-- we will also add show, edit, and delete buttons -->

</tr>
    </tbody>
@endforeach


@stop
