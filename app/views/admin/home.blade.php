@extends('layouts.bootstrap')

@section('title')
Welcome to the Admin
@stop

@section('content')

<div class="welcome">
    <h1>{{ Lang::get('adminmessage.introduction') }}</h1>
</div>

@stop

