@extends('layouts.bootstrap')

@section('content')

<h1>Edit {{ $item->title }}</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::model($item, array('route' => array($resource . '.update', $item->id), 'method' => 'PUT')) }}

<div class="form-group">
    {{ Form::label('title', 'Titel:') }}
    {{ Form::text('title', $item->title, array('class' => 'form-control')) }}
</div>
<br>


<div class="form-group">

    {{ Form::label('content', 'Content:') }}
    {{ Form::textarea('content', $item->content, array('class' => 'form-control')) }}
</div>
<br>
<br>

{{ Form::submit('Edit the ' . ucfirst($resource) . '!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
@stop

