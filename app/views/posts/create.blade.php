@extends('layouts.bootstrap')

@section('content')

<h1>Create a new {{ ucfirst($resource) }}</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

<!--wordt opgevangen in PostController.store -->
{{ Form::open(array('url' => $resource)) }}

<div class="form-group">
    {{ Form::label('title', 'Titel:') }}
    {{ Form::text('title', null, array('class' => 'form-control', 'placeholder' =>  'enter title')) }}
</div>

<div class="form-group">

    {{ Form::label('content', 'Content:') }}
    {{ Form::textarea('content', null, array('class' => 'form-control')) }}
</div>

{{ Form::submit('Create ' . ucfirst($resource) . '!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
@stop

