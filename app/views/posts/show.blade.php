@extends('layouts.bootstrap')

@section('content')

    <h2>Show {{ $item->title }}</h2>
    <p>
        <strong>Title:</strong> {{ $item->title }}<br>
        <strong>Content:</strong> {{ $item->content }}<br>
        <strong>Author:</strong> {{ $item->user->name }}<br>
        <button type="button" class="btn btn-success">Add Comment</button>
        @include('comments._list', array('list' => $item->comments))
    </p>
@stop