<?php
Route::get(
    'session/all',
    function () {
        $value = Session::all();
        var_dump($value);
        // return Redirect::route('home');
    }
);

Route::get(
    'session/put/{key}/{value}',
    function ($key, $value) {
        Session::put($key, $value);
        return Redirect::route('home');
    }
);

Route::get(
    'session/get/{key}',
    function ($key) {
        $value = Session::get($key);
        return "The values from session: $value";
        //return Redirect::route('home');
    }
);

Route::get(
    'session/flush',
    function () {
        Session::flush();
        return Redirect::route('home');
    }
);